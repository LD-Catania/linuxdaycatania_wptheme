<?php


class InitWebsite extends Timber\Site
{
    public function __construct()
    {
        add_action('after_setup_theme', array($this, 'theme_supports'));
        add_filter('timber/context', array($this, 'add_to_context'));
        add_filter('timber/twig', array($this, 'add_to_twig'));
        add_action('init', array($this, 'register_post_types'));
        add_action('init', array($this, 'register_taxonomies'));
        add_filter( 'wp_enqueue_scripts', array($this,'ldc_enqueue_scripts'));
        parent::__construct();
    }

    public function register_post_types()
    {
    }

    public function register_taxonomies()
    {
    }

    public function add_to_context($context)
    {
        $context['menu'] = new Timber\Menu();
        $context['site'] = $this;
        $context['latest_post'] = Timber::get_posts($this->get_latest_post(3));
        return $context;
    }

    public function theme_supports()
    {
        add_theme_support('automatic-feed-links');

        add_theme_support('title-tag');

        add_theme_support('post-thumbnails');

        add_theme_support(
            'html5',
            array(
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            )
        );

        add_theme_support(
            'post-formats',
            array(
                'aside',
                'image',
                'video',
                'quote',
                'link',
                'gallery',
                'audio',
            )
        );

        add_theme_support('menus');
    }

    public function add_to_twig($twig)
    {
        $twig->addExtension(new Twig_Extension_StringLoader());

        return $twig;
    }

    public function get_latest_post($howMany)
    {
        return new WP_Query(array(
            'posts_per_page' => $howMany,
        ));
    }
    public function ldc_enqueue_scripts(){
        wp_enqueue_script('app', get_stylesheet_directory_uri() . '/assets/dist/app.js', array('jquery'), 1.0, true);
	    wp_enqueue_script('roba', get_stylesheet_directory_uri() . '/static/site.js', array('jquery'), 1.0, true);

    }
}
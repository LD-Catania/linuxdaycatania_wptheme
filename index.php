<?php

use Timber\Timber;

$context = Timber::context();
$templates = array( 'pages/index.twig' );
if ( is_home() ) {
    array_unshift( $templates, 'front-page.twig', 'home.twig' );
}
Timber::render( $templates, $context );

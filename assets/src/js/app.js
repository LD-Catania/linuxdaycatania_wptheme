import Vue from 'vue'
import MeloButton from './components/MeloButton';
import vueSmoothScroll from 'vue-smooth-scroll'

Vue.use(vueSmoothScroll);
Vue.component('melo-button', MeloButton);

const app = new Vue({
    delimiters: ['${', '}'],
    el: '#app',
    data: {
            isTerminalEnabled:false,
    },
    methods: {
        toggleTerminal() {
            this.isTerminalEnabled = !this.isTerminalEnabled
        }
    },
    components:{
        MeloButton
    }
});



